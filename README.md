Introduction
============

ccg is a program which enables you to plot ERSL atmospheric gas data from the US National Oceanic and Atmospheric Administration (NOAA) web site.

Example data sources:

``` bash
mkdir data
cd data
wget ftp://ftp.cmdl.noaa.gov/data/trace_gases/co2/flask/co2_flask_surface_*.tar.gz
wget ftp://ftp.cmdl.noaa.gov/data/trace_gases/ch4/flask/ch4_flask_surface_*.tar.gz
tar -xzvf co2_flask_surface*.tar.gz
mv surface co2
tar -xzvf ch4_flask_surface*.tar.gz
mv surface ch4
```

The compressed files should be unzipped to a directory, then the name of the directory can be specified as a parameter.

Also you will need the list of observation sites:

``` bash
wget http://www.esrl.noaa.gov/gmd/dv/site/site_table.html
```

And emissions data:

``` bash
wget http://cdiac.ornl.gov/ftp/ndp030/CSV-FILES/global.1751_2010.csv
```

Installation
============

First install the prerequisites. On a Debian based system:

``` bash
sudo apt-get install build-essential gnuplot marble
```

Or on Arch/Parabola:

``` bash
sudo pacman -S gnuplot wget marble
```

To compile the command line program:

``` bash
make
sudo make install
```


Examples
========

In these examples the CO2 events were uncompressed to a directory called "co2" and the CH4 events were uncompressed to a directory called "ch4".

To plot methane in the northern hemisphere:

``` bash
ccg --sites sites.html --dir ch4 --filename methane.png --gas ch4 --start 1995 --end 2019 --latitudes "0N,90N" --label "nmol mol-1"
```

or to plot the same data monthly:

``` bash
ccg --sites sites.html --dir ch4 --filename methane.png --gas ch4 --start 1995 --end 2019 --latitudes "0N,90N" --monthly --label "nmol mol-1"
```

or the rate of change:

``` bash
ccg --sites sites.html --dir ch4 --filename methane.png --gas ch4 --start 1995 --end 2019 --latitudes "0N,90N" --change --label "nmol mol-1"
```

To plot CO2 between 1970 and 2019:

``` bash
ccg --sites sites.html --dir co2 --filename co2.png --gas co2 --start 1970 --end 2019 --label "Parts Per Million"
```

To plot the relationship between CO2 and altitude:

``` bash
ccg --sites sites.html --dir co2 --filename co2.png --gas co2 --start 1970 --end 2019 --altitudes --label "Parts Per Million"
```

A distribution scatter plot can also be produced so that you can get an idea of the range of measurements:

``` bash
ccg --sites sites.html --dir co2 --filename test.png --gas co2 --start 2007 --end 2019 --distribution --latitudes "10N,90N" --label "Parts Per Million"
```

Viewing on the Globe
====================

To export the sampling sites in KML format so that they can be viewed with Marble or Google Earth:

``` bash
ccg --sites site_table.html --kmlsites sites.kml
```
