/*
  Copyright (C) 2014,2018 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "timeseries.h"

void ccg_time_series::update_running_average()
{
    runningaverage.clear();
    runningaverage.push_back(mean[0]);
    for (int i = 1; i < (int)mean.size(); i++) {
        if (!monthly) {
            runningaverage.push_back((runningaverage[i-1]*0.5f)+
                                     (mean[i]*0.5f));
        }
        else {
            runningaverage.push_back((runningaverage[i-1]*0.95f)+
                                     (mean[i]*0.05f));
        }
    }
}

void ccg_time_series::update_monthly(vector<ccgdata> &data,
                                     vector<float> &mean,
                                     vector<float> &runningaverage,
                                     vector<float> &min,
                                     vector<float> &max)
{
    int index, i;
    int max_index = (end_year - start_year)*12;
    vector<float> av_min, av_max;
    vector<unsigned int> hits, av_min_hits, av_max_hits;

    mean.clear();
    min.clear();
    max.clear();

    for (i = 0; i <= max_index; i++) {
        av_min.push_back(0.0f);
        av_max.push_back(0.0f);
        av_min_hits.push_back((unsigned int)0);
        av_max_hits.push_back((unsigned int)0);
        mean.push_back(0.0f);
        min.push_back(0.0f);
        max.push_back(0.0f);
        hits.push_back((unsigned int)0);
    }

    for (i = 0; i < data.size(); i++) {
        // if the year is out of range
        if ((data[i].year < start_year) || (data[i].year > end_year)) {
            continue;
        }
        // calculate the array index
        index = ((data[i].year - start_year)*12) + data[i].month - 1;
        if ((index < 0) || (index > max_index)) {
            continue;
        }
        // update the mean value
        mean[index] += data[i].measured_value;
        // update the min and max values
        if (hits[index] > 0) {
            if (data[i].measured_value < min[index]) {
                min[index] = data[i].measured_value;
            }
            if (data[i].measured_value > max[index]) {
                max[index] = data[i].measured_value;
            }
        }
        else {
            min[index] = data[i].measured_value;
            max[index] = data[i].measured_value;
        }
        hits[index]++;
    }

    // update the means
    for (i = 0; i <= max_index; i++) {
        if (hits[i] > 0) {
            mean[i] /= (float)hits[i];
        }
        else {
            // no measurements for this month
            mean[i] = TIME_SERIES_NO_VALUE;
        }
    }

    for (i = 0; i < data.size(); i++) {
        // if the year is out of range
        if ((data[i].year < start_year) || (data[i].year > end_year)) {
            continue;
        }
        // calculate the array index
        index = ((data[i].year - start_year)*12) + data[i].month - 1;
        if ((index < 0) || (index > max_index)) {
            continue;
        }
        if (data[i].measured_value < mean[index]) {
            av_min[index] += data[i].measured_value;
            av_min_hits[index]++;
        }
        if (data[i].measured_value > mean[index]) {
            av_max[index] += data[i].measured_value;
            av_max_hits[index]++;
        }
    }
    for (i = 0; i <= max_index; i++) {
        if (av_min_hits[i] > 0) {
            min[i] = av_min[i] / av_min_hits[i];
        }
        if (av_max_hits[i] > 0) {
            max[i] = av_max[i] / av_max_hits[i];
        }
    }
}

void ccg_time_series::update_yearly(vector<ccgdata> &data)
{
    int index, i;
    int max_index = (end_year - start_year);
    vector<float> av_min, av_max;
    vector<unsigned int> hits, av_min_hits, av_max_hits;

    mean.clear();
    min.clear();
    max.clear();

    for (i = 0; i <= max_index; i++) {
        av_min.push_back(0.0f);
        av_max.push_back(0.0f);
        av_min_hits.push_back((unsigned int)0);
        av_max_hits.push_back((unsigned int)0);
        mean.push_back(0.0f);
        min.push_back(0.0f);
        max.push_back(0.0f);
        hits.push_back((unsigned int)0);
    }

    for (i = 0; i < data.size(); i++) {
        // if the year is out of range
        if ((data[i].year < start_year) || (data[i].year > end_year)) {
            continue;
        }
        // calculate the array index
        index = data[i].year - start_year;
        if ((index < 0) || (index > max_index)) {
            continue;
        }
        // update the mean value
        mean[index] += data[i].measured_value;
        // update the min and max values
        if (hits[index] > 0) {
            if (data[i].measured_value < min[index]) {
                min[index] = data[i].measured_value;
            }
            if (data[i].measured_value > max[index]) {
                max[index] = data[i].measured_value;
            }
        }
        else {
            min[index] = data[i].measured_value;
            max[index] = data[i].measured_value;
        }
        hits[index]++;
    }

    // update the means
    for (i = 0; i <= max_index; i++) {
        if (hits[i] > 0) {
            mean[i] /= (float)hits[i];
        }
        else {
            // no measurements for this month
            mean[i] = TIME_SERIES_NO_VALUE;
        }
    }

    for (i = 0; i < data.size(); i++) {
        // if the year is out of range
        if ((data[i].year < start_year) || (data[i].year > end_year)) {
            continue;
        }
        // calculate the array index
        index = data[i].year - start_year;
        if ((index < 0) || (index > max_index)) {
            continue;
        }
        if (data[i].measured_value < mean[index]) {
            av_min[index] += data[i].measured_value;
            av_min_hits[index]++;
        }
        if (data[i].measured_value > mean[index]) {
            av_max[index] += data[i].measured_value;
            av_max_hits[index]++;
        }
    }
    for (i = 0; i <= max_index; i++) {
        if (av_min_hits[i] > 0) {
            min[i] = av_min[i] / av_min_hits[i];
        }
        if (av_max_hits[i] > 0) {
            max[i] = av_max[i] / av_max_hits[i];
        }
    }

    update_running_average();
}

void ccg_time_series::update_yearly_variance(vector<ccgdata> &data)
{
    int i, i2, j;
    int max_index = (end_year - start_year);
    float variance_min, variance_max;
    vector<float> monthly_mean;
    vector<float> monthly_runningaverage;
    vector<float> monthly_min;
    vector<float> monthly_max;

    // calculate monthly averages
    update_monthly(data,
                   monthly_mean,
                   monthly_runningaverage,
                   monthly_min, monthly_max);

    mean.clear();
    min.clear();
    max.clear();

    for (i = 0; i <= max_index; i++) {
        mean.push_back(0.0f);
        min.push_back(0.0f);
        max.push_back(0.0f);
    }

    // update the variances
    for (i = 0; i <= max_index; i++) {
        variance_min = -1;
        variance_max = -1;
        for (j = 0; j < 12; j++) {
            i2 = i*12 + j;
            if (monthly_mean.size() < i2) continue;
            if ((int)monthly_mean[i2] ==
                (int)TIME_SERIES_NO_VALUE) continue;
            if ((variance_min == -1) ||
                (monthly_mean[i2] < variance_min)) {
                variance_min = monthly_mean[i2];
            }
            if ((variance_max == -1) ||
                (monthly_mean[i2] > variance_max)) {
                variance_max = monthly_mean[i2];
            }
        }
        if ((variance_max > variance_min) &&
            (variance_min != -1) &&
            (variance_max != -1)) {
            min[i] = variance_min;
            max[i] = variance_max;
            mean[i] = variance_max - variance_min;
        }
        else {
            // no measurements for this month
            mean[i] = TIME_SERIES_NO_VALUE;
        }
    }

    update_running_average();
}

// updates the minimum and maximum range values
// this is used for determining the raneg of y axis coordinates
// when drawing the graph
void ccg_time_series::update_range(bool use_minmax, float tollerance)
{
    // calculate the minimum and maximum range
    range_min = 99999;
    range_max = -99999;
    for (int i = 0; i < mean.size(); i++) {
        if (abs((int)mean[i]) != abs((int)TIME_SERIES_NO_VALUE)) {
            if (mean[i] < range_min) {
                range_min = mean[i];
            }
            if (mean[i] > range_max) {
                range_max = mean[i];
            }

            if (use_minmax) {
                if (min[i] < range_min) {
                    range_min = min[i];
                }
                if (min[i] > range_max) {
                    range_max = min[i];
                }

                if (max[i] < range_min) {
                    range_min = max[i];
                }
                if (max[i] > range_max) {
                    range_max = max[i];
                }
            }
        }
    }
    float range = range_max - range_min;
    range_min = range_min - (range*tollerance);
    range_max = range_max + (range*tollerance);
}

// updates the minimum and maximum range values
// this is used for determining the range of y axis coordinates
// when drawing the graph
void ccg_time_series::update_range_rate_of_change(bool use_minmax,
                                                  float tollerance)
{
    int offset = 1;
    float mean_val = 0,min_val = 0,max_val = 0;

    if (monthly) offset = 12;

    // calculate the minimum and maximum range
    range_min = 999999;
    range_max = -999999;
    for (int i = offset; i < mean.size(); i++) {
        if ((mean[i-offset] != 0.0f) &&
            (abs((int)mean[i]) != abs((int)TIME_SERIES_NO_VALUE)) &&
            (abs((int)mean[i-offset]) != abs((int)TIME_SERIES_NO_VALUE))) {
            mean_val = (mean[i] - mean[i-offset])*100/mean[i-offset];
        }

        if ((min[i-offset] != 0.0f) &&
            (abs((int)min[i]) != abs((int)TIME_SERIES_NO_VALUE)) &&
            (abs((int)min[i-offset]) != abs((int)TIME_SERIES_NO_VALUE))) {
            min_val = (min[i] - min[i-offset])*100/min[i-offset];
        }
        if ((max[i-offset] != 0.0f) &&
            (abs((int)max[i]) != abs((int)TIME_SERIES_NO_VALUE)) &&
            (abs((int)max[i-offset]) != abs((int)TIME_SERIES_NO_VALUE))) {
            max_val = (max[i] - max[i-offset])*100/max[i-offset];
        }

        if (mean_val < range_min) {
            range_min = mean_val;
        }
        if (mean_val > range_max) {
            range_max = mean_val;
        }

        if (use_minmax) {
            if (min_val < range_min) {
                range_min = min_val;
            }
            if (min_val > range_max) {
                range_max = min_val;
            }

            if (max_val < range_min) {
                range_min = max_val;
            }
            if (max_val > range_max) {
                range_max = max_val;
            }
        }
    }
    float range = range_max - range_min;
    range_min = range_min - (range*tollerance);
    range_max = range_max + (range*tollerance);
}

ccg_time_series::ccg_time_series(string gas, int start_year,
                                 int end_year,
                                 vector<ccgdata> &data,
                                 bool monthly, bool variance,
                                 vector<float> &area)
{
    this->start_year = start_year;
    this->end_year = end_year;
    this->monthly = monthly;
    this->variance = variance;
    this->gas = gas;

    for (int i = 0; i < (int)area.size(); i++) {
        this->area.push_back(area[i]);
    }

    if ((monthly) && (!variance)) {
        update_monthly(data,
                       this->mean, this->runningaverage,
                       this->min, this->max);
        update_running_average();
    }
    else {
        if (!variance) {
            update_yearly(data);
        }
        else {
            update_yearly_variance(data);
        }
    }

    // calculate the minimum and maximum range
    update_range(false, 0.1f);
}

// saves the time series to file
bool ccg_time_series::save(string filename, string separator)
{
    FILE * fp;
    int year = start_year;
    int month = 0, i, ctr;

    fp = fopen(filename.c_str(),"w");
    if (!fp) return false;

    for (i = mean.size()-1; i > 0; i--) {
        if (abs((int)mean[i]) !=
            abs((int)TIME_SERIES_NO_VALUE)) break;
    }
    ctr = i+1;

    if (monthly) {
        // monthly values
        for (i = 0; i < ctr; i++) {
            if (abs((int)mean[i]) !=
                abs((int)TIME_SERIES_NO_VALUE)) {
                fprintf(fp,"%.3f%s%.8f%s%.8f%s%.8f%s%.8f\n",
                        year + (month/12.0f), separator.c_str(),
                        mean[i], separator.c_str(),
                        min[i], separator.c_str(),
                        max[i], separator.c_str(),
                        runningaverage[i]);
            }
            month++;
            if (month>=12) {
                month=0;
                year++;
            }
        }
    }
    else {
        // yearly values
        for (i = 0; i < ctr; i++) {
            if (abs((int)mean[i]) !=
                abs((int)TIME_SERIES_NO_VALUE)) {
                fprintf(fp,"%d%s%.8f%s%.8f%s%.8f%s%.8f\n",
                        year, separator.c_str(),
                        mean[i], separator.c_str(),
                        min[i], separator.c_str(),
                        max[i], separator.c_str(),
                        runningaverage[i]);
            }
            year++;
        }
    }

    fclose(fp);
    return true;
}

// saves the rate of change time series to file
bool ccg_time_series::save_rate_of_change(string filename, string separator)
{
    FILE * fp;
    int year = start_year+1;
    int month = 0, i, ctr;
    float mean_val = 0, min_val = 0, max_val = 0;
    float running;

    fp = fopen(filename.c_str(),"w");
    if (!fp) return false;

    for (i = mean.size()-1; i > 0; i--) {
        if (abs((int)mean[i]) !=
            abs((int)TIME_SERIES_NO_VALUE)) break;
    }
    ctr = i+1;

    if (monthly) {
        // monthly values
        for (i = 12; i < ctr; i++) {

            if ((mean[i-12] != 0.0f) &&
                (abs((int)mean[i]) != abs((int)TIME_SERIES_NO_VALUE)) &&
                (abs((int)mean[i-12]) != abs((int)TIME_SERIES_NO_VALUE))) {
                mean_val = (mean[i]-mean[i-12])*100/mean[i-12];
            }

            if ((min[i-12] != 0.0f) &&
                (abs((int)min[i]) != abs((int)TIME_SERIES_NO_VALUE)) &&
                (abs((int)min[i-12]) != abs((int)TIME_SERIES_NO_VALUE))) {
                min_val = (min[i]-min[i-12])*100/min[i-12];
            }

            if ((max[i-12] != 0.0f) &&
                (abs((int)max[i]) != abs((int)TIME_SERIES_NO_VALUE)) &&
                (abs((int)max[i-12]) != abs((int)TIME_SERIES_NO_VALUE))) {
                max_val = (max[i]-max[i-12])*100/max[i-12];
            }

            if (i > 12) {
                running = (running*0.95f) + (mean_val*0.05f);
            }
            else {
                running = mean_val;
            }

            fprintf(fp,"%.3f%s%.8f%s%.8f%s%.8f%s%.8f\n",
                    year + (month/12.0f), separator.c_str(),
                    mean_val, separator.c_str(),
                    min_val, separator.c_str(),
                    max_val, separator.c_str(),
                    running);
            month++;
            if (month >= 12) {
                month = 0;
                year++;
            }
        }
    }
    else {
        // yearly values
        for (i = 1; i < ctr; i++) {
            if ((mean[i-1] != 0.0f) &&
                (abs((int)mean[i]) != abs((int)TIME_SERIES_NO_VALUE)) &&
                (abs((int)mean[i-1]) != abs((int)TIME_SERIES_NO_VALUE))) {
                mean_val = (mean[i]-mean[i-1])*100/mean[i-1];
            }
            if ((min[i-1] != 0.0f) &&
                (abs((int)min[i]) != abs((int)TIME_SERIES_NO_VALUE)) &&
                (abs((int)min[i-1]) != abs((int)TIME_SERIES_NO_VALUE))) {
                min_val = (min[i]-min[i-1])*100/min[i-1];
            }
            if ((max[i-1] != 0.0f) &&
                (abs((int)max[i]) != abs((int)TIME_SERIES_NO_VALUE)) &&
                (abs((int)max[i-1]) != abs((int)TIME_SERIES_NO_VALUE))) {
                max_val = (max[i]-max[i-1])*100/max[i-1];
            }

            if (i > 1) {
                running = (running*0.5f) + (mean_val*0.5f);
            }
            else {
                running = mean_val;
            }

            fprintf(fp,"%d%s%.8f%s%.8f%s%.8f%s%.8f\n",
                    year, separator.c_str(),
                    mean_val, separator.c_str(),
                    min_val, separator.c_str(),
                    max_val, separator.c_str(),
                    running);
            year++;
        }
    }

    fclose(fp);
    return true;
}
