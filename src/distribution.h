/*
  Functions for plotting gas measurement distributions
  Copyright (C) 2014,2018 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CCG_DISTRIBUTION_H
#define CCG_DISTRIBUTION_H

#include <stdio.h>
#include <vector>
#include <string>
#include <math.h>
#include "ccgdata.h"
#include "timeseries.h"

using namespace std;

class ccg_distribution {
 public:
    static bool save(vector<ccgdata> &data,
                     string filename, string separator);

    static void get_range(vector<ccgdata> &data,
                          float &min_measured_value,
                          float &max_measured_value);
};

#endif
