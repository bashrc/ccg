/*
  Uses gnuplot to show time series
  Copyright (C) 2014,2018 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CCG_GNUPLOT_H
#define CCG_GNUPLOT_H

#include <stdio.h>
#include <vector>
#include <string.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <math.h>
#include "globals.h"
#include "ccgdata.h"
#include "timeseries.h"
#include "altitudes.h"
#include "distribution.h"
#include "emissions.h"

using namespace std;

class ccg_gnuplot {
 private:
    static void create_script(string plot_script_filename,
                              string plot_data_filename,
                              string title, string subtitle,
                              float subtitle_indent_horizontal,
                              float subtitle_indent_vertical,
                              float min_x, float max_x,
                              float min_y, float max_y,
                              string x_label, string y_label,
                              string image_filename,
                              int image_width, int image_height,
                              string field_name, int field_number,
                              bool show_minmax, bool plot_points,
                              bool runningaverage);
    static string get_title(ccg_time_series * series);
    static string get_title(string gas,
                            int start_year, int end_year,
                            vector<float> &area);
    static string get_subtitle();
    static string get_subtitle(ccg_time_series * series);
    static string get_latitude_string(float lat);
    static string get_longitude_string(float lng);
    static int altitude_plot(vector<ccgdata> &data,
                             int max_altitude, int altitude_interval,
                             vector<float> &plot);
    static bool save_altitudes(string filename, string separator,
                               int altitude_interval,
                               vector<float> &plot);

 public:
    static int mean(ccg_time_series * series,
                    string image_filename,
                    int image_width, int image_height,
                    bool show_minmax,
                    float subtitle_indent_horizontal,
                    float subtitle_indent_vertical,
                    bool runningaverage,
                    string axis_label);

    static int rate_of_change(ccg_time_series * series,
                              string image_filename,
                              int image_width, int image_height,
                              bool show_minmax,
                              float subtitle_indent_horizontal,
                              float subtitle_indent_vertical,
                              bool runningaverage);

    static int altitude(string gas,
                        int start_year, int end_year,
                        vector<ccgdata> &data, vector<float> &area,
                        int altitude_interval,
                        string image_filename,
                        int image_width, int image_height,
                        float subtitle_indent_horizontal,
                        float subtitle_indent_vertical,
                        string axis_label);

    static int distribution(string gas, int start_year, int end_year,
                            vector<float> &area,
                            vector<ccgdata> &data,
                            string image_filename,
                            int image_width, int image_height,
                            float subtitle_indent_horizontal,
                            float subtitle_indent_vertical,
                            string axis_label);
    static int emissions(ccg_time_series * series,
                         string image_filename,
                         int image_width, int image_height,
                         bool show_minmax,
                         float subtitle_indent_horizontal,
                         float subtitle_indent_vertical,
                         bool runningaverage,
                         string axis_label);
    static int emissions_change(ccg_time_series * series,
                                string image_filename,
                                int image_width, int image_height,
                                bool show_minmax,
                                float subtitle_indent_horizontal,
                                float subtitle_indent_vertical,
                                bool runningaverage,
                                string axis_label);
};

#endif
