/*
    Copyright (C) 2014,2018 Bob Mottram
    bob@freedombone.net

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef EMISSIONS_H_
#define EMISSIONS_H_

#include <stdio.h>
#include <vector>
#include <string.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <math.h>
#include "globals.h"
#include "ccgdata.h"

using namespace std;

/* See http://cdiac.ornl.gov/faq.html#Q6
   Using 5.137 x 1018 kg as the mass of the atmosphere
   (Trenberth, 1981 JGR 86:5238-46),
   1 ppmv of CO2= 2.13 Gt of carbon
 */
#define MEGATONS_PER_PPM 213000000

class emissions {
private:
    static bool FileExists(string filename);
public:
    static void load(string filename,
                     int start_year,
                     int end_year,
                     vector<ccgdata> &data);

    emissions();
    virtual ~emissions();
};

#endif /* EMISSIONS_H_ */
