/*
    Copyright (C) 2014,2018 Bob Mottram
    bob@freedombone.net

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ccg.h"

ccg::ccg() {
}

ccg::~ccg() {
}

bool ccg::site_has_property(int property_type, string value,
                            string site_code,
                            vector<sitedata> &sites)
{
    for (int i = 0; i < (int)sites.size(); i++) {
        if (sites[i].site_code == site_code) {
            //switch (property_type) {
            //}
        }
    }
    return false;
}


bool ccg::vector_contains(vector<int> &list, int num_to_find)
{
    vector<int>::iterator result;

    result = find(list.begin(), list.end(), num_to_find);
    if (result == list.end()) {
        return(false);
    }
    else {
        return(true);
    }
}

bool ccg::vector_contains(vector<string> &list, string str_to_find)
{
    vector<string>::iterator result;

    result = find(list.begin(), list.end(), str_to_find);
    if (result == list.end()) {
        return(false);
    }
    else {
        return(true);
    }
}

bool ccg::vector_contains(vector<long long int> &list,
                          long long int num_to_find)
{
    vector<long long int>::iterator result;

    result = find(list.begin(), list.end(), num_to_find);
    if (result == list.end()) {
        return(false);
    }
    else {
        return(true);
    }
}

bool ccg::FileExists(string filename)
{
    ifstream file;
    file.open(filename.c_str());

    // Check if the file exists
    if (file.is_open() == true) {
        file.close();
        return true;
    }

    return(false);
}

void ccg::save_sites(string kml_filename,
                     vector<sitedata> &sites)
{
    ofstream kmlfile;
    kmlfile.open(kml_filename.c_str());
    kmlfile << "<?xml version=" << '"' << "1.0" <<
        '"' << " encoding='UTF-8'?>\n";
    kmlfile << "<kml xmlns=" << '"' <<
        "http://www.opengis.net/kml/2.2" << '"' << ">\n";
    kmlfile << "<Document>\n";
    for (int i = 0; i < (int)sites.size(); i++) {
        kmlfile << "  <Placemark>\n";
        kmlfile << "    <name>" << sites[i].name << "</name>\n";
        kmlfile << "    <description>Site code " << sites[i].site_code;
        kmlfile << "</description>\n";
        kmlfile << "    <Point>\n";
        kmlfile << "      <coordinates>" <<
            -sites[i].longitude << "," << sites[i].latitude <<
            "," << sites[i].altitude << "</coordinates>\n";
        kmlfile << "    </Point>\n";
        kmlfile << "  </Placemark>\n";
    }
    kmlfile << "</Document>\n";
    kmlfile << "</kml>\n";
    kmlfile.close();
}

bool ccg::get_site_location(string site_code,
                            vector<sitedata> &sites,
                            float &north,
                            float &west,
                            float &altitude)
{
    for (int i = 0; i < (int)sites.size(); i++) {
        if (sites[i].site_code == site_code) {
            north = sites[i].latitude;
            west = sites[i].longitude;
            altitude = sites[i].altitude;
            return true;
        }
    }
    return false;
}

bool ccg::site_within_area(string site_code,
                           vector<sitedata> &sites,
                           float latitudeN1,
                           float longitudeW1,
                           float latitudeN2,
                           float longitudeW2)
{
    float north=0,west=0,altitude=0;

    if (!get_site_location(site_code, sites,
                           north,west,altitude)) {
        return false;
    }

    if (latitudeN2 < latitudeN1) {
        float temp = latitudeN1;
        latitudeN1 = latitudeN2;
        latitudeN2 = temp;
    }

    if ((longitudeW1 == 0.0f) &&
        (longitudeW2 == 0.0f)) {
        // site between two latitudes
        if ((north >= latitudeN1) &&
            (north <= latitudeN2)) {
            return true;
        }
    }
    else {
        // site within an area
        if (longitudeW2 < longitudeW1) {
            float temp = longitudeW1;
            longitudeW1 = longitudeW2;
            longitudeW2 = temp;
        }

        float w = longitudeW2 - longitudeW1;
        if (w > 180) w = 360 - w;
        if (w < -180) w = -360 + w;
        float h = latitudeN2 - latitudeN1;
        if (h > 180) h = 360 - h;
        if (h < -180) h = -360 + h;
        float cx = longitudeW1 + (w/2);
        float cy = latitudeN1 + (h/2);
        float r1 = w/2;
        float r2 = h/2;

        float dy = north - cy;
        if (dy > 180) dy = 360 - dy;
        if (dy < -180) dy = -360 + dy;
        if (fabs(dy) < r2) {
            float dx = west - cx;
            if (dx > 180) dy = 360 - dx;
            if (dx < -180) dy = -360 + dx;
            if (fabs(dx) < r1) {
                return true;
            }
        }
    }
    return false;
}

void ccg::get_sites_within_area(float latitudeN1,
                                float longitudeW1,
                                float latitudeN2,
                                float longitudeW2,
                                vector<sitedata> &sites,
                                vector<sitedata> &returned_site_numbers)
{
    if (latitudeN2 < latitudeN1) {
        float temp = latitudeN1;
        latitudeN1 = latitudeN2;
        latitudeN2 = temp;
    }

    if ((longitudeW1 == 0.0f) &&
        (longitudeW2 == 0.0f)) {
        // sites between two latitudes
        for (int i = 0; i < (int)sites.size(); i++) {
            if ((sites[i].latitude >= latitudeN1) &&
                (sites[i].latitude <= latitudeN2)) {
                returned_site_numbers.push_back(sites[i]);
            }
        }
    }
    else {
        // sites within an area
        if (longitudeW2 < longitudeW1) {
            float temp = longitudeW1;
            longitudeW1 = longitudeW2;
            longitudeW2 = temp;
        }

        float w = longitudeW2 - longitudeW1;
        if (w > 180) w = 360 - w;
        if (w < -180) w = -360 + w;
        float h = latitudeN2 - latitudeN1;
        if (h > 180) h = 360 - h;
        if (h < -180) h = -360 + h;
        float cx = longitudeW1 + (w/2);
        float cy = latitudeN1 + (h/2);
        float r1 = w/2;
        float r2 = h/2;

        for (int i = 0; i < (int)sites.size(); i++) {
            float dy = sites[i].latitude - cy;
            if (dy > 180) dy = 360 - dy;
            if (dy < -180) dy = -360 + dy;
            if (fabs(dy) < r2) {
                float dx = sites[i].longitude - cx;
                if (dx > 180) dy = 360 - dx;
                if (dx < -180) dy = -360 + dx;
                if (fabs(dx) < r1) {
                    returned_site_numbers.push_back(sites[i]);
                }
            }
        }
    }
}

bool ccg::site_code_in_data(string site_code,
                            vector<sitedata> &sites)
{
    for (int i = 0; i < (int)sites.size(); i++) {
        if (sites[i].site_code == site_code) return true;
    }
    return false;
}

bool ccg::site_exists(vector<sitedata> &sites,
                      string site_code)
{
    for (int i = 0; i < (int)sites.size(); i++) {
        if (sites[i].site_code == site_code) {
            return true;
        }
    }
    return false;
}

// returns a valid site code scraped from HTML
string ccg::valid_site_code(string s)
{
    const char * str = s.c_str();
    string site_code = "";

    for (int i = 0; i < s.length(); i++) {
        if (((str[i]>='A') && (str[i]<='Z')) ||
            ((str[i]>='0') && (str[i]<='9'))) {
            site_code += str[i];
        }
        else {
            if (site_code.length() > 0) break;
        }
    }
    return site_code;
}

// given a HTML formatted string return the unformatted content
string ccg::remove_html_formatting(string s)
{
    int i, state = 0;
    string result = "";

    for (i = 0; i < s.length(); i++) {
        switch (state) {
        case 0: {
            if (s.substr(i,1) == "<") {
                state++;
            }
            else {
                result += s.substr(i,1);
            }
            break;
        }
        case 1: {
            if (s.substr(i,1) == ">") {
                state=0;
            }
            break;
        }
        }
    }
    return result;
}

void ccg::load_sites(string filename,
                     vector<sitedata> &sites)
{
    bool reading_table = false;
    bool reading_row = false;
    int field_index = 0, offset, startpos, endpos;
    sitedata site;
    string str = "", str2;
    ifstream iFile(filename.c_str());

    while (!iFile.eof()) {
        getline(iFile, str);

        if (str.length() > 0) {
            offset = 0;
            while (offset < str.length()) {
                if (str.substr(offset,1) == "<") break;
                offset++;
            }
            if (!reading_table) {
                if (str.length() >= 6) {
                    if (offset+1 < str.length()-6) {
                        if ((str.substr(offset+1,6) == "TABLE ") ||
                            (str.substr(offset+1,6) == "table ")) {
                            reading_table = true;
                            continue;
                        }
                    }
                }
            }
            if ((!reading_row) && (reading_table)) {
                if ((strstr(str.c_str(),"<TR")!=NULL) ||
                    (strstr(str.c_str(),"<tr")!=NULL)) {
                    reading_row = true;
                }
            }
            if (reading_row) {
                if (str.substr(offset+1,2) == "td") {
                    startpos = offset+1;
                    while ((startpos < str.length()-1) &&
                           (str.substr(startpos,1) != ">")) {
                        startpos++;
                    }
                    if (startpos < str.length()-5) {
                        endpos = startpos+1;
                        while ((endpos < str.length()-1) &&
                               (str.substr(endpos,5) != "</td>")) {
                            endpos++;
                        }
                        if ((endpos < str.length()-1) &&
                            ((endpos-1)-(startpos+1)+1 > 0)) {
                            startpos++;
                            endpos--;
                            str2 = remove_html_formatting(str.substr(startpos,
                                                                     endpos-
                                                                     startpos+1));

                            switch(field_index) {
                            case 0: {
                                str2 = valid_site_code(str2);
                                if (str2.length() >= 3) {
                                    site.site_code = str2;
                                }
                                else {
                                    site.site_code = "";
                                }
                                break;
                            }
                            case 1: {
                                site.name = str2;
                                break;
                            }
                            case 2: {
                                site.latitude = atof(str2.c_str());
                                break;
                            }
                            case 3: {
                                site.longitude = atof(str2.c_str());
                                break;
                            }
                            case 4: {
                                site.altitude = atof(str2.c_str());
                                break;
                            }
                            case 5: {
                                site.country = str2;
                                break;
                            }
                            }

                            field_index++;
                        }
                    }
                }
                if ((strstr(str.c_str(),"</TR")!=NULL) ||
                    (strstr(str.c_str(),"</tr")!=NULL)) {
                    reading_row = false;
                    field_index = 0;
                    sites.push_back(site);
                }
                if ((strstr(str.c_str(),"</TABLE")!=NULL) ||
                    (strstr(str.c_str(),"</table")!=NULL)) {
                    reading_table = false;
                    reading_row = false;
                    field_index = 0;
                }
            }
        }
    }
    iFile.close();
}

void ccg::load_sites(string filename,
                     float ccg_version,
                     vector<sitedata> &sites)
{
    if (FileExists(filename)) {
        load_sites(filename, sites);
    }
}

//
string ccg::trim_string(string str)
{
    const char * cstr = str.c_str();
    string result = "";

    for (int i = 0; i < strlen(cstr); i++) {
        if (result == "") {
            if ((cstr[i] != ' ') && (cstr[i] != '\t')) {
                result += cstr[i];
            }
        }
        else {
            result += cstr[i];
        }
    }

    str = "";
    for (int i = result.length()-1; i >= 0; i--) {
        if (str == "") {
            if ((result.substr(i,1) != " ") &&
                (result.substr(i,1) != "\t")) {
                str = result.substr(i,1) + str;
            }
        }
        else {
            str = result.substr(i,1) + str;
        }
    }
    return str;
}

// is the given site within the area?
bool ccg::site_within_area(float north, float west,
                           vector<float> &area)
{
    float latitudeN1 = area[0];
    float longitudeW1 = area[1];
    float latitudeN2 = area[2];
    float longitudeW2 = area[3];

    if (latitudeN2 < latitudeN1) {
        float temp = latitudeN1;
        latitudeN1 = latitudeN2;
        latitudeN2 = temp;
    }

    if ((longitudeW1 == 0.0f) &&
        (longitudeW2 == 0.0f)) {
        // station between two latitudes
        if ((north >= latitudeN1) &&
            (north <= latitudeN2)) {
            return true;
        }
    }
    else {
        // station within an area
        if (longitudeW2 < longitudeW1) {
            float temp = longitudeW1;
            longitudeW1 = longitudeW2;
            longitudeW2 = temp;
        }

        float w = longitudeW2 - longitudeW1;
        if (w > 180) w = 360 - w;
        if (w < -180) w = -360 + w;
        float h = latitudeN2 - latitudeN1;
        if (h > 180) h = 360 - h;
        if (h < -180) h = -360 + h;
        float cx = longitudeW1 + (w/2);
        float cy = latitudeN1 + (h/2);
        float r1 = w/2;
        float r2 = h/2;

        float dy = north - cy;
        if (dy > 180) dy = 360 - dy;
        if (dy < -180) dy = -360 + dy;
        if (fabs(dy) < r2) {
            float dx = west - cx;
            if (dx > 180) dy = 360 - dx;
            if (dx < -180) dy = -360 + dx;
            if (fabs(dx) < r1) {
                return true;
            }
        }
    }
    return false;
}

void ccg::load(float data_version,
               string filename,
               int start_year,
               int end_year,
               vector<string> &site_codes,
               string gas,
               string method,
               vector<ccgdata> &data,
               vector<sitedata> &sites,
               vector<float> &area)
{
    if (data_version == 1.0) {
        load_V1(filename, start_year, end_year,
                site_codes, gas, method, data, sites, area);
    }
}

void ccg::load_V1(string filename,
                  int start_year,
                  int end_year,
                  vector<string> &site_codes,
                  string gas,
                  string method,
                  vector<ccgdata> &data,
                  vector<sitedata> &sites,
                  vector<float> &area)
{

    ifstream iFile(filename.c_str());
    char method_ch = ' ';

    if (method.length() > 0) {
        method_ch = toupper(method.c_str()[0]);
    }

    string str;
    while (!iFile.eof()) {
        getline(iFile, str);

        if (str != "") {
            if ((str.substr(0,1)=="#") || (str.length() < 137)) {
                continue;
            }

            string site_code = str.substr(0,3);
            int year = atoi(str.substr(4,4).c_str());
            int month = atoi(str.substr(9,2).c_str());
            int day = atoi(str.substr(12,2).c_str());
            int hour = atoi(str.substr(15,2).c_str());
            int min = atoi(str.substr(18,2).c_str());
            int sec = atoi(str.substr(21,2).c_str());
            string flask_ID = trim_string(str.substr(24,8));
            string measurement_method = str.substr(33,1);
            string trace_gas_name = trim_string(str.substr(35,8));
            string measurement_group = trim_string(str.substr(44,7));
            float measured_value = atof(str.substr(53,9).c_str());
            float estimated_uncertainty = atof(str.substr(63,9).c_str());
            string qc_flag = trim_string(str.substr(73,3));
            string instrument = trim_string(str.substr(77,3));
            int measurement_year = atoi(str.substr(81,4).c_str());
            int measurement_month = atoi(str.substr(86,2).c_str());
            int measurement_day = atoi(str.substr(89,2).c_str());
            int measurement_hour = atoi(str.substr(92,2).c_str());
            int measurement_min = atoi(str.substr(95,2).c_str());
            int measurement_sec = atoi(str.substr(98,2).c_str());
            float latitude = atof(str.substr(101,8).c_str());
            float longitude = atof(str.substr(110,9).c_str());
            float altitude = atof(str.substr(120,8).c_str());
            unsigned long event_number = atoi(str.substr(129,8).c_str());

            /* the desired gas type */
            if ((measured_value < -998) || (trace_gas_name != gas)) {
                continue;
            }

            /* the desired measurement method */
            if ((method != "any") && (method != "all") && (method != "")) {
                if (measurement_method.length()>0) {
                    if (measurement_method.c_str()[0] != method_ch) {
                        continue;
                    }
                }
                else {
                    continue;
                }
            }

            /* check that the sample is within the desired area */
            if (area.size()==4) {
                if (!site_within_area(latitude, -longitude, area)) {
                    continue;
                }
            }

            if (site_code_in_data(site_code, sites)) {

                if ((year >= start_year) && (year <= end_year)) {

                    for (int m = 0; m < 12; m++) {
                        ccgdata data_point;
                        strncpy(data_point.site_code, site_code.c_str(),
                                SITE_CODE_LENGTH);
                        data_point.year = year;
                        data_point.month = month;
                        data_point.day = day;
                        data_point.hour = hour;
                        data_point.minute = min;
                        data_point.second = sec;
                        strcpy(data_point.flask_ID, flask_ID.c_str());
                        data_point.method = measurement_method.c_str()[0];
                        strcpy(data_point.gas_identifier,
                               trace_gas_name.c_str());
                        strcpy(data_point.measurement_group,
                               measurement_group.c_str());
                        data_point.measured_value = measured_value;
                        data_point.estimated_uncertainty =
                            estimated_uncertainty;
                        strncpy(data_point.qc_flag, qc_flag.c_str(), 3);
                        strncpy(data_point.instrument, instrument.c_str(), 2);
                        data_point.measurement_year = measurement_year;
                        data_point.measurement_month = measurement_month;
                        data_point.measurement_day = measurement_day;
                        data_point.measurement_hour = measurement_hour;
                        data_point.measurement_minute = measurement_min;
                        data_point.measurement_second = measurement_sec;
                        data_point.latitude = latitude;
                        data_point.longitude = longitude;
                        data_point.altitude = altitude;
                        data_point.event_number = event_number;

                        data.push_back(data_point);
                    }
                }
            }
        }
    }

    iFile.close();
}
