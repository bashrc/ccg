/*
  Creates a time series
  Copyright (C) 2014,2018 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CCG_TIMESERIES_H
#define CCG_TIMESERIES_H

#include <stdio.h>
#include <vector>
#include <string.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <math.h>
#include "globals.h"
#include "ccgdata.h"

using namespace std;

#define TIME_SERIES_NO_VALUE -9999

class ccg_time_series {
 private:
    vector<float> mean;
    vector<float> runningaverage;
    vector<float> min;
    vector<float> max;

    void update_running_average();
    void update_monthly(vector<ccgdata> &data,
                        vector<float> &mean,
                        vector<float> &runningaverage,
                        vector<float> &min,
                        vector<float> &max);
    void update_yearly(vector<ccgdata> &data);
    void update_yearly_variance(vector<ccgdata> &data);
 public:
    int start_year, end_year;
    bool monthly, variance;
    float range_min, range_max;
    string gas;
    vector<float> area;

    void update_range(bool use_minmax, float tollerance);
    void update_range_rate_of_change(bool use_minmax, float tollerance);

    bool save(string filename, string separator);
    bool save_rate_of_change(string filename, string separator);

    ccg_time_series(string gas, int start_year, int end_year,
                    vector<ccgdata> &data,
                    bool monthly, bool variance,
                    vector<float> &area);
};

#endif
