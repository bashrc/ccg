/*
  Copyright (C) 2014,2018 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CCG_GLOBALS_H_
#define CCG_GLOBALS_H_

#define VERSION 1.00

#define CCG_DATA_SOURCE "ftp://ftp.cmdl.noaa.gov/data"

#ifndef _WIN32
    #define CCGGRAPH_BINARY "/usr/bin/ccg"
    #define COPY_FILE "cp"
    #define DELETE_FILE "rm -f"
    #define MAKE_DIR "mkdir"
    #define PATH_SEPARATOR "/"
#else
    #define CCGGRAPH_BINARY "C:\ccg"
    #define COPY_FILE "copy"
    #define DELETE_FILE "del"
    #define MAKE_DIR "mkdir"
    #define PATH_SEPARATOR "\\"
#endif

#define SITE_CODE_LENGTH            10
#define MEASUREMENT_GROUP_LENGTH    16
#define QC_FLAGS                     3
#define INSTRUMENT_TYPE_LENGTH       2
#define FLASK_ID_LENGTH              9
#define GAS_IDENTIFIER_LENGTH        8

enum {
    CCG_GRAPH_MEAN = 0,
    CCG_GRAPH_ALTITUDES,
    CCG_GRAPH_RATE_OF_CHANGE,
    CCG_GRAPH_DISTRIBUTION,
    CCG_GRAPH_EMISSIONS,
    CCG_GRAPH_EMISSIONS_CHANGE,
};

#endif
