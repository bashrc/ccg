/*
  Site data
  See http://www.esrl.noaa.gov/gmd/dv/site/site_table.html
  Copyright (C) 2014,2018 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CCG_SITEDATA_H_
#define CCG_SITEDATA_H_

#include <string>
#include "globals.h"

using namespace std;

class sitedata {
public:
    string site_code;
    string name;
    float latitude, longitude, altitude;
    string country;

    sitedata();
    virtual ~sitedata();
};

#endif /* SITEDATA_H_ */
