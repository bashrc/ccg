/*
  plots climate change gasses
  see ftp://ftp.cmdl.noaa.gov
  Copyright (C) 2014,2018 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CCG_DATA_H_
#define CCG_DATA_H_

#include "globals.h"

class ccgdata {
public:
    char site_code[SITE_CODE_LENGTH];
    int year, month, day;
    int hour, minute, second;
    char flask_ID[FLASK_ID_LENGTH];
    char method;
    char gas_identifier[GAS_IDENTIFIER_LENGTH];
    char measurement_group[MEASUREMENT_GROUP_LENGTH];
    float measured_value;
    float estimated_uncertainty;
    char qc_flag[QC_FLAGS];
    char instrument[INSTRUMENT_TYPE_LENGTH];
    int measurement_year, measurement_month, measurement_day;
    int measurement_hour, measurement_minute, measurement_second;
    float latitude, longitude, altitude;
    unsigned long event_number;

    ccgdata();
    virtual ~ccgdata();
};

#endif /* CCGDATA_H_ */
