/*
    Copyright (C) 2014,2018 Bob Mottram
    bob@freedombone.net

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CCG_H_
#define CCG_H_

#include <stdio.h>
#include <vector>
#include <string.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <math.h>
#include "globals.h"
#include "ccgdata.h"
#include "sitedata.h"

using namespace std;

enum {
    ENTITY_SITE = 0
};

class ccg {
private:
    static string remove_html_formatting(string s);
    static string valid_site_code(string s);

    static bool site_exists(vector<sitedata> &sites,
                            string site_code);

    static bool site_code_in_data(string site_code,
                                  vector<sitedata> &sites);

    static void load_sites(string filename,
                           vector<sitedata> &sites);

    static void load_V1(string filename,
                        int start_year,
                        int end_year,
                        vector<string> &site_codes,
                        string gas,
                        string method,
                        vector<ccgdata> &data,
                        vector<sitedata> &sites,
                        vector<float> &area);

    static bool site_within_area(float north, float west,
                                 vector<float> &area);

public:
    static string trim_string(string str);

    static bool site_has_property(int property_type,string value,
                                  string site_code,
                                  vector<sitedata> &sites);

    static bool FileExists(string filename);

    // Returns the location for the given site number
    static bool get_site_location(string site_code,
                                  vector<sitedata> &sites,
                                  float &north,
                                  float &west,
                                  float &altitude);

    // Returns true if the given site number is within the given area
    static bool site_within_area(string site_code,
                                 vector<sitedata> &sites,
                                 float latitudeN1,
                                 float longitudeW1,
                                 float latitudeN2,
                                 float longitudeW2);

    static bool vector_contains(vector<int> &list, int num_to_find);

    static bool vector_contains(vector<string> &list, string str_to_find);

    static bool vector_contains(vector<long long int> &list,
                                long long int num_to_find);

    static void get_sites_within_area(float latitudeN1,
                                      float longitudeW1,
                                      float latitudeN2,
                                      float longitudeW2,
                                      vector<sitedata> &sites,
                                      vector<sitedata> &returned_site_numbers);

    static void load_sites(string filename,
                           float ccg_version,
                           vector<sitedata> &sites);

    static void save_sites(string kml_filename,
                           vector<sitedata> &sites);

    static void load(float data_version,
                     string filename,
                     int start_year,
                     int end_year,
                     vector<string> &site_codes,
                     string gas,
                     string method,
                     vector<ccgdata> &data,
                     vector<sitedata> &sites,
                     vector<float> &area);

    ccg();
    virtual ~ccg();
};

#endif /* CCG_H_ */
