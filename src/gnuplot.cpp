/*
  Copyright (C) 2014,2018 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gnuplot.h"

const string plot_script_filename = "/tmp/ccg.plot";
const string plot_data_filename = "/tmp/ccg.dat";

void ccg_gnuplot::create_script(string plot_script_filename,
                                string plot_data_filename,
                                string title, string subtitle,
                                float subtitle_indent_horizontal,
                                float subtitle_indent_vertical,
                                float min_x, float max_x,
                                float min_y, float max_y,
                                string x_label, string y_label,
                                string image_filename,
                                int image_width, int image_height,
                                string field_name, int field_number,
                                bool show_minmax, bool plot_points,
                                bool runningaverage)
{
    string draw_type = "lines";

    if (plot_points) draw_type = "points";

    ofstream plotfile;
    plotfile.open(plot_script_filename.c_str());
    plotfile << "reset\n";
    plotfile << "set title \"" << title << "\"\n";
    if (subtitle != "") {
        plotfile << "set label \"" << subtitle << "\" at screen " <<
            subtitle_indent_horizontal << ", screen " <<
            subtitle_indent_vertical << "\n";
    }
    plotfile << "set xrange [" << min_x << ":" << max_x << "]\n";
    plotfile << "set yrange [" << min_y << ":" << max_y << "]\n";
    plotfile << "set lmargin 9\n";
    plotfile << "set rmargin 2\n";
    plotfile << "set xlabel \"" << x_label << "\"\n";
    plotfile << "set ylabel \"" << y_label << "\"\n";
    plotfile << "set grid\n";
    plotfile << "set key right bottom\n";

    string file_type = "png";
    if (image_filename != "") {
        if ((image_filename.substr((int)image_filename.size()-3,3) == "jpg") ||
            (image_filename.substr((int)image_filename.size()-4,4) == "jpeg")) {
            file_type = "jpeg";
        }
        else {
            file_type = image_filename.substr((int)image_filename.size()-3,3);
        }

        plotfile << "set terminal " << file_type << " size " <<
            image_width << "," << image_height << "\n";

        plotfile << "set output \"" << image_filename << "\"\n";
        plotfile << "plot ";

        plotfile << "\"" << plot_data_filename <<
            "\" using 1:" << field_number;
        if ((show_minmax) || (runningaverage)) {
            plotfile << " title \"" << field_name << "\" with " << draw_type;
        }
        else {
            plotfile << " notitle with " << draw_type << "\n";
        }

        if (runningaverage) {
            plotfile << ", \"" << plot_data_filename <<
                "\" using 1:5 title \"Running\" with " << draw_type;
        }

        if (show_minmax) {
            plotfile << ", \"" << plot_data_filename <<
                "\" using 1:3 title \"Min\" with " << draw_type;
            plotfile << ", \"" << plot_data_filename <<
                "\" using 1:4 title \"Max\" with " << draw_type;
        }

        plotfile << "\n";
    }

    plotfile.close();
}

string ccg_gnuplot::get_latitude_string(float lat)
{
    char str[64];
    if (lat>=0) {
        sprintf(str,"%dN", (int)lat);
    }
    else {
        sprintf(str,"%dS", (int)-lat);
    }
    return std::string(str);
}

string ccg_gnuplot::get_longitude_string(float lng)
{
    char str[64];
    if (lng>=0) {
        sprintf(str,"%dW", (int)lng);
    }
    else {
        sprintf(str,"%dE", (int)-lng);
    }
    return std::string(str);
}

// get the graph title
string ccg_gnuplot::get_title(string gas, int start_year,
                              int end_year, vector<float> &area)
{
    char str[256],str2[256];
    char gasstr2[64];
    const char * gasstr = gas.c_str();
    int i;

    // convert gas name to upper case
    for (i = 0; i < gas.length(); i++) {
        gasstr2[i] = toupper(gasstr[i]);
    }
    gasstr2[i]=0;

    sprintf(str,"Atmospheric %s %d-%d",
            gasstr2, start_year, end_year);
    if (area.size()==4) {
        if ((area[1]==0.0f) && (area[3]==0.0f)) {
            sprintf(str2,"%s between latitudes %s - %s",
                    str, get_latitude_string(area[0]).c_str(),
                    get_latitude_string(area[2]).c_str());
        }
        else {
            sprintf(str2,"%s within area %s %s - %s %s",
                    str, get_latitude_string(area[0]).c_str(),
                    get_longitude_string(area[1]).c_str(),
                    get_latitude_string(area[2]).c_str(),
                    get_longitude_string(area[3]).c_str());
        }
        strcpy(str,str2);
    }
    return std::string(str);
}

// get the graph title
string ccg_gnuplot::get_title(ccg_time_series * series)
{
    return get_title(series->gas, series->start_year,
                     series->end_year, series->area);
}

string ccg_gnuplot::get_subtitle()
{
    char str[256];

    sprintf(str,"Source %s",CCG_DATA_SOURCE);
    return std::string(str);
}

// get the graph subtitle
string ccg_gnuplot::get_subtitle(ccg_time_series * series)
{
    return get_subtitle();
}

// plots the mean value over time
int ccg_gnuplot::mean(ccg_time_series * series,
                      string image_filename,
                      int image_width, int image_height,
                      bool show_minmax,
                      float subtitle_indent_horizontal,
                      float subtitle_indent_vertical,
                      bool runningaverage,
                      string axis_label)
{
    string title = get_title(series);
    string subtitle = get_subtitle(series);
    string vertical_title = "Mean";

    if (series->variance) {
        vertical_title = "Seasonal Variance";
        show_minmax = false;
    }

    if (!series->save(plot_data_filename,"   ")) return -1;

    create_script(plot_script_filename,
                  plot_data_filename,
                  title, subtitle,
                  subtitle_indent_horizontal,
                  subtitle_indent_vertical,
                  series->start_year, series->end_year,
                  series->range_min, series->range_max,
                  "Year", axis_label,
                  image_filename,
                  image_width, image_height,
                  vertical_title, 2, show_minmax, false,
                  runningaverage);

    string command_str = "gnuplot " + plot_script_filename;
    return system(command_str.c_str());
}


int ccg_gnuplot::altitude(string gas,
                          int start_year, int end_year,
                          vector<ccgdata> &data, vector<float> &area,
                          int altitude_interval,
                          string image_filename,
                          int image_width, int image_height,
                          float subtitle_indent_horizontal,
                          float subtitle_indent_vertical,
                          string axis_label)
{
    char str[256];
    string title = get_title(gas, start_year, end_year, area);
    string subtitle = get_subtitle();
    const int max_altitude = 30000;
    vector<float> plot;
    int max_index;
    float range_min = 999999;
    float range_max = -999999;

    max_index = ccg_altitudes::plot(data, max_altitude,
                                    altitude_interval, plot);

    // calculate the range of values
    for (int i = 0; i <= max_index; i++) {
        if (plot[i] == 0.0f) continue;
        if (plot[i] < range_min) {
            range_min = plot[i];
        }
        if (plot[i] > range_max) {
            range_max = plot[i];
        }
    }

    ccg_altitudes::save(plot_data_filename, "   ",
                        altitude_interval, plot);

    create_script(plot_script_filename,
                  plot_data_filename,
                  title, subtitle,
                  subtitle_indent_horizontal,
                  subtitle_indent_vertical,
                  0, max_index*altitude_interval,
                  range_min, range_max,
                  "Altitude (metres)", axis_label,
                  image_filename,
                  image_width, image_height,
                  "Mean", 2, false, false, false);

    string command_str = "gnuplot " + plot_script_filename;
    return system(command_str.c_str());
}

// plots the rate of change over time
int ccg_gnuplot::rate_of_change(ccg_time_series * series,
                                string image_filename,
                                int image_width, int image_height,
                                bool show_minmax,
                                float subtitle_indent_horizontal,
                                float subtitle_indent_vertical,
                                bool runningaverage)
{
    string title = get_title(series);
    string subtitle = get_subtitle(series);

    if (!series->save_rate_of_change(plot_data_filename,"   ")) return -1;

    create_script(plot_script_filename,
                  plot_data_filename,
                  title, subtitle,
                  subtitle_indent_horizontal,
                  subtitle_indent_vertical,
                  series->start_year, series->end_year,
                  series->range_min, series->range_max,
                  "Year", "Rate of Change %",
                  image_filename,
                  image_width, image_height,
                  "Rate of Change", 2, show_minmax, false,
                  runningaverage);

    string command_str = "gnuplot " + plot_script_filename;
    return system(command_str.c_str());
}

int ccg_gnuplot::distribution(string gas, int start_year, int end_year,
                              vector<float> &area,
                              vector<ccgdata> &data,
                              string image_filename,
                              int image_width, int image_height,
                              float subtitle_indent_horizontal,
                              float subtitle_indent_vertical,
                              string axis_label)
{
    string title = get_title(gas, start_year, end_year, area);
    string subtitle = get_subtitle();
    float range_min=0;
    float range_max=0;

    if (!ccg_distribution::save(data,plot_data_filename,"   ")) return -1;

    ccg_distribution::get_range(data, range_min, range_max);

    create_script(plot_script_filename,
                  plot_data_filename,
                  title, subtitle,
                  subtitle_indent_horizontal,
                  subtitle_indent_vertical,
                  start_year, end_year,
                  range_min, range_max,
                  "Year", axis_label,
                  image_filename,
                  image_width, image_height,
                  "Mean", 2, false, true, false);

    string command_str = "gnuplot " + plot_script_filename;
    return system(command_str.c_str());
}

int ccg_gnuplot::emissions(ccg_time_series * series,
                           string image_filename,
                           int image_width, int image_height,
                           bool show_minmax,
                           float subtitle_indent_horizontal,
                           float subtitle_indent_vertical,
                           bool runningaverage,
                           string axis_label)
{
    char titlestr[256];
    string title = "";
    string subtitle = "Source: http://cdiac.ornl.gov/ftp/ndp030";
    string vertical_title = "Total Emissions (ppm)";

    axis_label="Total Emissions (ppm)";
    sprintf(titlestr,"Total CO2 emissions %d-%d", series->start_year, series->end_year);
    title += titlestr;

    if (!series->save(plot_data_filename,"   ")) return -1;

    create_script(plot_script_filename,
                  plot_data_filename,
                  title, subtitle,
                  subtitle_indent_horizontal,
                  subtitle_indent_vertical,
                  series->start_year, series->end_year,
                  series->range_min, series->range_max,
                  "Year", axis_label,
                  image_filename,
                  image_width, image_height,
                  vertical_title, 2, show_minmax, false,
                  runningaverage);

    string command_str = "gnuplot " + plot_script_filename;
    return system(command_str.c_str());
}

int ccg_gnuplot::emissions_change(ccg_time_series * series,
                                  string image_filename,
                                  int image_width, int image_height,
                                  bool show_minmax,
                                  float subtitle_indent_horizontal,
                                  float subtitle_indent_vertical,
                                  bool runningaverage,
                                  string axis_label)
{
    char titlestr[256];
    string title = "";
    string subtitle = "Source: http://cdiac.ornl.gov/ftp/ndp030";
    string vertical_title = "Total Emissions (% change)";

    axis_label="Yearly Rate of Change %";
    sprintf(titlestr,"Change in Total CO2 emissions %d-%d", series->start_year, series->end_year);
    title += titlestr;

    if (!series->save_rate_of_change(plot_data_filename,"   ")) return -1;

    create_script(plot_script_filename,
                  plot_data_filename,
                  title, subtitle,
                  subtitle_indent_horizontal,
                  subtitle_indent_vertical,
                  series->start_year, series->end_year,
                  series->range_min, series->range_max,
                  "Year", axis_label,
                  image_filename,
                  image_width, image_height,
                  vertical_title, 2, show_minmax, false,
                  runningaverage);

    string command_str = "gnuplot " + plot_script_filename;
    return system(command_str.c_str());
}
