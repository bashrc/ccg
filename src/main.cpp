/*
  A program for plotting trends in atmospheric gasses
  Copyright (C) 2014-2018 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <algorithm>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <regex.h>
#include <stdio.h>
#include <errno.h>
#include <err.h>
#include <stdlib.h>
#include <ctype.h>

#include "anyoption.h"
#include "directory.h"
#include "globals.h"
#include "parse.h"
#include "ccgdata.h"
#include "sitedata.h"
#include "ccg.h"
#include "emissions.h"
#include "timeseries.h"
#include "gnuplot.h"
#include "googleearth.h"

using namespace std;

static void get_data_files(string data_directory, vector<string> &filenames)
{
    int retval =
        walk_dir(data_directory, ".\\.$",
                 WS_DOTFILES|WS_DEFAULT|WS_MATCHDIRS,filenames);
    switch(retval) {
    case WALK_BADIO: {
        fprintf(stderr,"Directory '%s' Error %d: %s\n",
                data_directory.c_str(),errno,strerror(errno));
        break;
    }
    case WALK_NAMETOOLONG: {
        fprintf(stderr,"Directory name '%s' is too long\n",
                data_directory.c_str());
        break;
    }
    case WALK_BADPATTERN: {
        fprintf(stderr,"Bad directory '%s'\n", data_directory.c_str());
        break;
    }
    }
    if ((retval==WALK_OK) && (filenames.size()==0)) {
        /* directory exists but contains no files */
        fprintf(stderr,"No files found within the directory '%s'\n",
                data_directory.c_str());
    }
}

int main(int argc, char* argv[]) {
    AnyOption *opt = new AnyOption();

    // help
    opt->addUsage("Example: ");
    opt->addUsage("  cgg --start 1970 --end 2012 --sites data/sites.html " \
                  "--dir data --filename test.png");
    opt->addUsage(" ");
    opt->addUsage("Usage: ");
    opt->addUsage("");
    opt->addUsage("     --start <year>              Starting year for the plot");
    opt->addUsage("     --end <year>                Ending year for the plot");
    opt->addUsage("     --gas <name>                " \
                  "The type of gas to be plotted, eg. co2");
    opt->addUsage("     --method <P|D|G|T|S|N|F>    " \
                  "Method by which the sample was taken");
    opt->addUsage("     --dir <directory>           " \
                  "Directory containing CGG data");
    opt->addUsage("     --dataversion <version>     " \
                  "Version number of the CCG data format");
    opt->addUsage("     --sites <file>              " \
                  "HTML file containing site codes and locations");
    opt->addUsage("     --sitecodes <list>          " \
                  "List of site codes to be plotted");
    opt->addUsage("     --filename <file>           " \
                  "Filename for the image to save");
    opt->addUsage("     --minmax                    " \
                  "Show minimum and maximum values");
    opt->addUsage("     --monthly                   " \
                  "Plot monthly rather than yearly");
    opt->addUsage("     --variance                  " \
                  "Plot yearly variance amplitude");
    opt->addUsage("     --area <N,W,N,W>            " \
                  "Plot within a geographical area");
    opt->addUsage("     --latitudes <min,max>       " \
                  "Plot within a min and max latitude band");
    opt->addUsage("     --altitudes                 Plot altitudes");
    opt->addUsage("     --change                    Plot rates of change");
    opt->addUsage("     --distribution              " \
                  "Plot the distribution of measurements");
    opt->addUsage("     --subhorizontal <value>     " \
                  "Horizontal position of the subtitle in the range 0.0 - 1.0");
    opt->addUsage("     --subvertical <value>       " \
                  "Vertical position of the subtitle in the range 0.0 - 1.0");
    opt->addUsage("     --runningaverage            Show a running average");
    opt->addUsage("     --label <name>              " \
                  "Label for the vertical axis of the graph");
    opt->addUsage("     --kmlsites <filename>       Save sites in KML format");
    opt->addUsage("     --kmlsamples <filename>     Save samples in KML format");
    opt->addUsage("     --emissions <csv filename>  Plot CO2 emissions from the given file");
    opt->addUsage("  -V --version                   Show version number");
    opt->addUsage("     --help                      Show help");
    opt->addUsage("");

    opt->setOption("start");
    opt->setOption("end");
    opt->setOption("year");
    opt->setOption("dir");
    opt->setOption("dataversion");
    opt->setOption("sites");
    opt->setOption("sitecodes");
    opt->setOption("gas");
    opt->setOption("method");
    opt->setOption("filename");
    opt->setOption("latitudes");
    opt->setOption("area");
    opt->setOption("subhorizontal");
    opt->setOption("subvertical");
    opt->setOption("kmlsites");
    opt->setOption("kmlsamples");
    opt->setOption("emissions");
    opt->setOption("label");
    opt->setFlag("version", 'V');
    opt->setFlag("help");
    opt->setFlag("minmax");
    opt->setFlag("monthly");
    opt->setFlag("variance");
    opt->setFlag("altitudes");
    opt->setFlag("change");
    opt->setFlag("distribution");
    opt->setFlag("runningaverage");

    opt->processCommandArgs(argc, argv);

    if ((!opt->hasOptions()) || opt->getFlag("help")) {
        // print usage if no options
        opt->printUsage();
        delete opt;
        return 0;
    }

    if ((opt->getFlag("version")) || (opt->getFlag('V'))) {
        printf("Version %.2f\n", VERSION);
        delete opt;
        return 0;
    }

    int altitude_interval = 250;

    // the upper and lower margin for the graph
    float tollerance = 0.1f;

    bool runningaverage = false;
    if (opt->getFlag("runningaverage")) {
        runningaverage = true;
    }

    float subtitle_indent_horizontal=0.4f;
    float subtitle_indent_vertical=0.94f;
    if (opt->getValue("subhorizontal")) {
        subtitle_indent_horizontal = atof(opt->getValue("subhorizontal"));
    }
    if (opt->getValue("subvertical")) {
        subtitle_indent_vertical = atof(opt->getValue("subvertical"));
    }

    string gas = "co2";
    if (opt->getValue("gas") != NULL) {
        gas = opt->getValue("gas");
    }

    int graph_type = CCG_GRAPH_MEAN;
    if (opt->getFlag("altitudes")) {
        graph_type = CCG_GRAPH_ALTITUDES;
    }
    if (opt->getFlag("distribution")) {
        graph_type = CCG_GRAPH_DISTRIBUTION;
    }
    string emissions_filename = "";
    if ((opt->getValue("emissions") != NULL) && (gas=="co2 emissions")) {
        emissions_filename = opt->getValue("emissions");
        if (opt->getFlag("change")) {
            graph_type = CCG_GRAPH_EMISSIONS_CHANGE;
        }
        else {
            graph_type = CCG_GRAPH_EMISSIONS;
        }
        gas = "co2";
    }
    else {
        if (opt->getFlag("change")) {
            graph_type = CCG_GRAPH_RATE_OF_CHANGE;
        }
    }

    bool monthly = false;
    if (opt->getFlag("monthly")) {
        monthly = true;
    }

    bool variance = false;
    if (opt->getFlag("variance")) {
        variance = true;
        // variance only applies for yearly cycles
        monthly = false;
    }

    bool show_minmax = false;
    if (opt->getFlag("minmax")) {
        show_minmax = true;
    }

    // label for the vertical axis
    string axis_label = string_to_upper(gas) + " Measurement";
    if (opt->getValue("label")) {
        axis_label = opt->getValue("label");
    }

    // measurement method
    string method = "";
    if (opt->getValue("method") != NULL) {
        method = opt->getValue("method");
    }

    float data_version = 1.0;
    if( opt->getValue( "dataversion" ) != NULL  ) {
        data_version = atof(opt->getValue("dataversion"));
    }

    vector<sitedata> sites;
    string sites_filename = "";
    if (opt->getValue("sites") != NULL) {
        sites_filename = opt->getValue("sites");

        ccg::load_sites(sites_filename,
                        data_version, sites);
    }
    if (sites.size()==0) {
        printf("No sites file specified\n");
        delete opt;
        return 0;
    }

    if (opt->getValue("kmlsites") != NULL) {
        string kml_filename = opt->getValue("kmlsites");
        ccg_googleearth::save_sites(kml_filename, sites);
        printf("Sites saved to %s\n",kml_filename.c_str());
        delete opt;
        return 0;
    }

    vector<string> site_codes;
    if (opt->getValue("sitecodes") != NULL) {
        string sites_str = opt->getValue("sitecodes");
        parse_string(sites_str, site_codes);
    }

    int start_year = 1969;
    if (opt->getValue("start") != NULL) {
        start_year = atoi(opt->getValue("start"));
    }

    int end_year = 2011;
    if (opt->getValue("end") != NULL) {
        end_year = atoi(opt->getValue("end"));
    }

    string ccg_directory = ".";
    if (opt->getValue("dir") != NULL) {
        ccg_directory = opt->getValue("dir");
    }

    string image_filename = "";
    if (opt->getValue("filename") != NULL) {
        image_filename = opt->getValue("filename");
    }
    if ((image_filename == "") && (opt->getValue("kmlsamples") == NULL)) {
        printf("No image specified.  Use the --filename option.\n");
        delete opt;
        return -1;
    }

    int image_width=1024;
    int image_height=800;
    if (opt->getValue("width") != NULL) {
        image_width = atoi(opt->getValue("width"));
    }
    if (opt->getValue("height") != NULL) {
        image_height = atoi(opt->getValue("height"));
    }

    // plot within an area
    vector<float> area;
    if (opt->getValue("area") != NULL) {
        string area_str = opt->getValue("area");
        parse_string(area_str, area);
        if (((int)area.size() > 0) && ((int)area.size() < 4)) {
            printf("Only %d out of 4 area coordinates were specified.  " \
                   "Did you miss out a comma?\n", (int)area.size());
            delete opt;
            return 0;
        }
    }

    //latitude range
    if (opt->getValue("latitudes") != NULL) {
        string latitude_str = opt->getValue("latitudes");
        float min_latitude=40, max_latitude=50;
        parse_latitude(latitude_str, min_latitude, max_latitude);
        area.clear();
        area.push_back(min_latitude);
        area.push_back(0.0f);
        area.push_back(max_latitude);
        area.push_back(0.0f);
    }


    // get a list of files within the specified data directory
    vector<string> filenames;
    get_data_files(ccg_directory, filenames);
    if (filenames.size()==0) {
        delete opt;
        return -1;
    }

    // load the data from the specified directory
    vector<ccgdata> data;
    printf("Loading...");
    fflush(stdout);
    if (graph_type != CCG_GRAPH_EMISSIONS) {
        for (int i = 0; i < filenames.size(); i++) {
            ccg::load(data_version, filenames[i],
                      start_year, end_year,
                      site_codes, gas, method, data, sites,
                      area);
        }
    }
    else {
        emissions::load(emissions_filename,
                        start_year, end_year, data);
    }

    if (data.size()==0) {
        printf("No data was loaded\n");
        return -1;
    }
    printf("%d data samples loaded\n",(int)data.size());

    if (opt->getValue("kmlsamples") != NULL) {
        string kml_filename = opt->getValue("kmlsamples");
        ccg_googleearth::save_samples(kml_filename, data);
        printf("Samples saved to %s\n",kml_filename.c_str());
        delete opt;
        return 0;
    }

    // all options parsed
    delete opt;

    switch(graph_type) {
    case CCG_GRAPH_MEAN: {
        // create a time series
        printf("Creating time series...");
        fflush(stdout);
        ccg_time_series series(gas, start_year, end_year, data,
                               monthly, variance, area);
        printf("Done\n");

        printf("Plotting graph...");
        fflush(stdout);
        series.update_range(show_minmax, tollerance);
        ccg_gnuplot::mean(&series, image_filename,
                          image_width, image_height,
                          show_minmax,
                          subtitle_indent_horizontal,
                          subtitle_indent_vertical,
                          runningaverage,
                          axis_label);
        printf("Done\n");
        break;
    }
    case CCG_GRAPH_RATE_OF_CHANGE: {
        // create a time series
        printf("Creating time series...");
        fflush(stdout);
        ccg_time_series series(gas, start_year, end_year, data,
                               monthly, variance, area);
        printf("Done\n");

        printf("Plotting graph...");
        fflush(stdout);
        series.update_range_rate_of_change(false, tollerance);
        ccg_gnuplot::rate_of_change(&series, image_filename,
                                    image_width, image_height,
                                    false,
                                    subtitle_indent_horizontal,
                                    subtitle_indent_vertical,
                                    runningaverage);
        printf("Done\n");
        break;
    }
    case CCG_GRAPH_ALTITUDES: {
        ccg_gnuplot::altitude(gas, start_year, end_year, data, area,
                              altitude_interval,
                              image_filename,
                              image_width, image_height,
                              subtitle_indent_horizontal,
                              subtitle_indent_vertical,
                              axis_label);
        break;
    }
    case CCG_GRAPH_DISTRIBUTION: {
        ccg_gnuplot::distribution(gas, start_year, end_year,
                                  area, data,
                                  image_filename,
                                  image_width, image_height,
                                  subtitle_indent_horizontal,
                                  subtitle_indent_vertical,
                                  axis_label);
        break;
    }
    case CCG_GRAPH_EMISSIONS_CHANGE: {
        printf("Creating time series...");
        fflush(stdout);
        ccg_time_series series(gas, start_year, end_year, data,
                               monthly, variance, area);
        printf("Done\n");

        printf("Plotting graph...");
        fflush(stdout);
        series.update_range_rate_of_change(false, tollerance);
        ccg_gnuplot::emissions_change(&series, image_filename,
                                      image_width, image_height,
                                      show_minmax,
                                      subtitle_indent_horizontal,
                                      subtitle_indent_vertical,
                                      runningaverage,
                                      axis_label);
        printf("Done\n");
        break;
    }
    case CCG_GRAPH_EMISSIONS: {
        printf("Creating time series...");
        fflush(stdout);
        ccg_time_series series(gas, start_year, end_year, data,
                               monthly, variance, area);
        printf("Done\n");

        printf("Plotting graph...");
        fflush(stdout);
        series.update_range(show_minmax, tollerance);
        ccg_gnuplot::emissions(&series, image_filename,
                               image_width, image_height,
                               show_minmax,
                               subtitle_indent_horizontal,
                               subtitle_indent_vertical,
                               runningaverage,
                               axis_label);
        printf("Done\n");
        break;
    }
    }

    return 0;
}
