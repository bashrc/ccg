/*
    Copyright (C) 2014-2018 Bob Mottram
    bob@freedombone.net

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "emissions.h"

emissions::emissions() {
}

emissions::~emissions() {
}

bool emissions::FileExists(string filename)
{
    ifstream file;
    file.open(filename.c_str());

    // Check if the file exists
    if (file.is_open() == true) {
        file.close();
        return true;
    }

    return(false);
}

void emissions::load(string filename,
                     int start_year,
                     int end_year,
                     vector<ccgdata> &data)
{
    if (!FileExists(filename)) {
        return;
    }

    ifstream iFile(filename.c_str());

    string str;
    while (!iFile.eof()) {
        getline(iFile, str);

        if (str.length() > 0) {
            if (str.substr(0,1)=="#") {
                continue;
            }

            int year=0, total=0, field_number = 0;
            string valuestr="";

            for (int i = 0; i < str.length(); i++) {
                if (str.substr(i,1)==",") {
                    switch(field_number) {
                    case 0: {
                        year = atoi(valuestr.c_str());
                        break;
                    }
                    case 1: {
                        total = atof(valuestr.c_str()) *
                            1000000.0f / (float)MEGATONS_PER_PPM;
                        break;
                    }
                    }
                    field_number++;
                    valuestr="";
                }
                else {
                    valuestr += str.substr(i,1);
                }
            }

            if ((year >= start_year) && (year <= end_year)) {
                ccgdata data_point;
                data_point.year = year;
                data_point.month = 1;
                data_point.day = 1;
                data_point.hour = 1;
                data_point.minute = 1;
                data_point.second = 1;
                data_point.measured_value = total;
                data.push_back(data_point);
            }
        }
    }

    iFile.close();
}
