/*
  Copyright (C) 2014,2018 Bob Mottram
  bob@freedombone.net

  This is a modified version of the recursive directory
  reading function from:
  http://rosettacode.org/wiki/Walk_Directory_Tree#Library:_POSIX
  Licensed under GNU FDL

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CCG_DIRECTORY_H
#define CCG_DIRECTORY_H

#include <string>
#include <string.h>
#include <vector>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <regex.h>
#include <stdio.h>
#include <errno.h>
#include <err.h>
#include <stdlib.h>
#include <ctype.h>

using namespace std;

enum {
    WALK_OK = 0,
    WALK_BADPATTERN,
    WALK_NAMETOOLONG,
    WALK_BADIO
};

#define WS_NONE     0
#define WS_RECURSIVE    (1 << 0)
#define WS_DEFAULT  WS_RECURSIVE
#define WS_FOLLOWLINK   (1 << 1)    /* follow symlinks */
#define WS_DOTFILES (1 << 2)    /* per unix convention, .file is hidden */
#define WS_MATCHDIRS    (1 << 3)    /* if pattern is used on dir names too */

int walk_dir(string dname, string  pattern, int spec,
             vector<string> &filenames);

#endif
