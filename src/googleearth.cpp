/*
  functions for use with Google Earth
  Copyright (C) 2014,2018 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "googleearth.h"

void ccg_googleearth::save_sites(
    string kml_filename,
    vector<sitedata> &sites)
{
    ofstream kmlfile;
    kmlfile.open(kml_filename.c_str());
    kmlfile << "<?xml version=" << '"' << "1.0" <<
        '"' << " encoding='UTF-8'?>\n";
    kmlfile << "<kml xmlns=" << '"' <<
        "http://www.opengis.net/kml/2.2" << '"' << ">\n";
    kmlfile << "<Document>\n";
    for (int i = 0; i < (int)sites.size(); i++) {
        kmlfile << "  <Placemark>\n";
        kmlfile << "    <name>" << sites[i].name << " (" <<
            sites[i].site_code << ")</name>\n";
        kmlfile << "    <Point>\n";
        kmlfile << "      <coordinates>" << sites[i].longitude <<
            "," << sites[i].latitude << "," << sites[i].altitude <<
            "</coordinates>\n";
        kmlfile << "    </Point>\n";
        kmlfile << "  </Placemark>\n";
    }
    kmlfile << "</Document>\n";
    kmlfile << "</kml>\n";
    kmlfile.close();
}

void ccg_googleearth::save_samples(
    string kml_filename,
    vector<ccgdata> &samples)
{
    ofstream kmlfile;
    kmlfile.open(kml_filename.c_str());
    kmlfile << "<?xml version=" << '"' << "1.0" <<
        '"' << " encoding='UTF-8'?>\n";
    kmlfile << "<kml xmlns=\"http://www.opengis.net/kml/2.2\"" \
        " xmlns:gx=\"http://www.google.com/kml/ext/2.2\">\n";
    kmlfile << "<Document>\n";
    for (int i = 0; i < (int)samples.size(); i++) {
        kmlfile << "  <Placemark>\n";
        kmlfile << "    <name>Sample " <<
            samples[i].event_number << "</name>\n";
        kmlfile << "    <description>";
        kmlfile << "Flask ID: " <<
            std::string(samples[i].flask_ID) << "\n";
        kmlfile << "Measured Value: " <<
            samples[i].measured_value << "\n";
        kmlfile << "</description>\n";
        kmlfile << "    <gx:TimeStamp><when>" <<
            samples[i].year << "-" << samples[i].month <<
            "-" << samples[i].day << "T" << samples[i].hour <<
            ":" << samples[i].minute << ":" << samples[i].second <<
            "-00:00</when></gx:TimeStamp>\n";
        kmlfile << "    <Point>\n";
        kmlfile << "      <coordinates>" << -samples[i].longitude <<
            "," << samples[i].latitude << "," <<
            samples[i].altitude << "</coordinates>\n";
        kmlfile << "    </Point>\n";
        kmlfile << "  </Placemark>\n";
    }
    kmlfile << "</Document>\n";
    kmlfile << "</kml>\n";
    kmlfile.close();
}
