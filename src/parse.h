/*
  Parsing functions
  Copyright (C) 2014,2018 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CCG_PARSE_H
#define CCG_PARSE_H

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>

using namespace std;

string string_to_lower(string str);
string string_to_upper(string str);
void parse_string(string str, vector<string> &v);
void parse_string(string str, vector<float> &v);
void parse_latitude(string str, float &min, float &max);
void parse_view(string str, float &longitude, float &latitude);
void parse_range(string str, float &min, float &max);

#endif
