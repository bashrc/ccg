/*
  Functions for plotting gas measurements at different altitudes
  Copyright (C) 2014,2018 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "altitudes.h"

// calculates the average value for each altitude interval
int ccg_altitudes::plot(vector<ccgdata> &data,
                        int max_altitude, int altitude_interval,
                        vector<float> &plot)
{
    vector<double> sum;
    vector<unsigned int> hits;
    int index,max=0;

    // create the array and populate it with zeros
    for (int i = 0; i < max_altitude/altitude_interval; i++) {
        sum.push_back(0.0f);
        hits.push_back(0);
    }

    for (int i = 0; i < (int)data.size(); i++) {
        // index within the array
        index = (int)(data[i].altitude / altitude_interval);
        // within the maximum range
        if (index < sum.size()) {
            sum[index] += data[i].measured_value;
            hits[index]++;
        }
    }

    // calculate the means
    plot.clear();
    for (int i = 0; i < max_altitude/altitude_interval; i++) {
        if (hits[i] > 0) {
            plot.push_back((float)(sum[i] / hits[i]));
            if (i>max) max = i;
        }
        else {
            plot.push_back(0.0f);
        }
    }
    return max;
}

// saves altitudes to a data file which can be used by gnuplot
bool ccg_altitudes::save(string filename, string separator,
                         int altitude_interval,
                         vector<float> &plot)
{
    FILE * fp;

    fp = fopen(filename.c_str(),"w");
    if (!fp) return false;

    for (int i = 0; i < plot.size(); i++) {
        fprintf(fp,"%d%s%.8f\n",
                i*altitude_interval, separator.c_str(),
                plot[i]);
    }

    fclose(fp);
    return true;
}
