/*
  functions for use with Google Earth
  Copyright (C) 2014,2018 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GOOGLE_EARTH_H
#define GOOGLE_EARTH_H

#include <stdio.h>
#include <vector>
#include <fstream>

#include "globals.h"
#include "sitedata.h"
#include "ccgdata.h"

using namespace std;

class ccg_googleearth {
 public:
    static void save_sites(string kml_filename,
                           vector<sitedata> &sites);

    static void save_samples(string kml_filename,
                             vector<ccgdata> &samples);
};

#endif
