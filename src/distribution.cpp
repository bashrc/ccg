/*
  Copyright (C) 2014,2018 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "distribution.h"

// saves data point to a file suitable for plotting with gnuplot
bool ccg_distribution::save(vector<ccgdata> &data,
                            string filename, string separator)
{
    FILE * fp;
    int i, ctr;

    fp = fopen(filename.c_str(),"w");
    if (!fp) return false;

    for (i = data.size()-1; i > 0; i--) {
        if (abs((int)data[i].measured_value) !=
            abs((int)TIME_SERIES_NO_VALUE)) break;
    }
    ctr = i+1;

    for (i = 0; i < ctr; i++) {
        fprintf(fp,"%.8f%s%.8f\n",
                data[i].year + (data[i].month/12.0f) +
                (data[i].hour/(24.0f*12.0f)) +
                (data[i].minute/(60.0f*24.0f*12.0f)),
                separator.c_str(),
                data[i].measured_value);
    }

    fclose(fp);
    return true;
}

// returns the range of values within the given data
void ccg_distribution::get_range(vector<ccgdata> &data,
                                 float &min_measured_value,
                                 float &max_measured_value)
{
    float min_val = 0, max_val = 0;
    unsigned int histogram[201];
    int i, max_hist_value = 0;

    min_measured_value = 0;
    max_measured_value = 0;

    for (i = 0; i < 200; i++) {
        histogram[i] = 0;
    }

    // get min amd max values
    for (i = 0; i < data.size(); i++) {
        if ((min_val == 0) || (data[i].measured_value < min_val)) {
            min_val = data[i].measured_value;
        }
        if ((max_val == 0) || (data[i].measured_value > max_val)) {
            max_val = data[i].measured_value;
        }
    }

    if (max_val == min_val) return;

    // update the histogram
    for (i = 0; i < data.size(); i++) {
        int index = (int)((data[i].measured_value - min_val)*
                          200/(max_val - min_val));
        histogram[index]++;
    }

    // get the maximum histogram value
    for (i = 0; i < 200; i++) {
        if (histogram[i] > max_hist_value) {
            max_hist_value = histogram[i];
        }
    }

    unsigned int threshold = data.size()*1/100;
    unsigned int points = 0;
    for (i = 0; i < 200; i++) {
        points += histogram[i];
        if (points > threshold) {
            min_measured_value = min_val + (i*(max_val-min_val)/200);
            break;
        }
    }
    points = 0;
    for (i = 199; i >= 0; i--) {
        points += histogram[i];
        if (points > threshold) {
            max_measured_value = min_val + (i*(max_val-min_val)/200);
            break;
        }
    }
}
