APP=ccg
VERSION=1.00

all:
	g++ -O3 -o $(APP) src/*.cpp -Isrc

debug:
	g++ -g -o $(APP) src/*.cpp -Isrc

source:
	tar -zcvpf ../$(APP)_$(VERSION).orig.tar.gz ../$(APP)-$(VERSION) --exclude-vcs

install:
	install -m 755 $(APP) /usr/bin/$(APP)
	install -m 644 man/$(APP).1.gz /usr/share/man/man1/$(APP).1.gz

clean:
	rm -f $(APP)
	rm -rf debian/$(APP)
	rm -f debian/$(APP).debhelper.log debian/$(APP).substvars
	rm -f ../$(APP)_*.deb ../$(APP)_*.asc ../$(APP)_*.dsc ../$(APP)*.changes ../$(APP)*.tar.gz
	rm -f *.plot plot* *.log
	rm -f *.png
	rm -f *.sh~
	rm -f *.deb \#* \.#*
	rm -f Makefile~
